package com.sheepApps.android.iamaslov.todo.view.pickerfragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;


import com.sheepApps.android.iamaslov.todo.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import static com.sheepApps.android.iamaslov.todo.view.AddEditNoteActivity.BUNDLE_DATE_OF_TASK;

public class DatePickerFragment extends DialogFragment {


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            String dateOfTask = bundle.getString(BUNDLE_DATE_OF_TASK, "");
            @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat(getResources().getString(R.string.DF));
            try {
                Date stringToDate = formatter.parse(dateOfTask);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(stringToDate);
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                return new DatePickerDialog(getActivity(),R.style.DialogThemeDate, (DatePickerDialog.OnDateSetListener) getActivity(), year, month, day);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(),R.style.DialogThemeDate, (DatePickerDialog.OnDateSetListener) getActivity(), year, month, day);
    }
}
