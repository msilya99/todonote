package com.sheepApps.android.iamaslov.todo.view;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sheepApps.android.iamaslov.todo.R;
import com.sheepApps.android.iamaslov.todo.databinding.DonelistRecyclerBinding;
import com.sheepApps.android.iamaslov.todo.databinding.RecyclerViewBinding;
import com.sheepApps.android.iamaslov.todo.model.Note;
import com.sheepApps.android.iamaslov.todo.viewmodel.DoneViewModel;
import com.sheepApps.android.iamaslov.todo.viewmodel.MenuViewModel;
import com.sheepApps.android.iamaslov.todo.viewmodel.RecyclerViewModel;

import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator;

import static com.sheepApps.android.iamaslov.todo.view.TabActivity.REQUEST_ADD_NOTE;
import static com.sheepApps.android.iamaslov.todo.view.TabActivity.REQUEST_EDIT_NOTE;

public class DoneListFragment extends Fragment {

    private DoneViewModel mDoneViewModel;

    public static final String PREV_FRAGMENT = "com.sheepApps.android.iamaslov.TODO.view.Note_view.PREV_FRAGMENT";
    private Resources res;
    private int mPrevFragment;
    private LinearLayoutManager mLinearLayoutManager;
    private DonelistRecyclerBinding mBinding;
    private View v;


    public static Fragment newInstance(int previouslyFragment) {
        DoneListFragment doneListFragment = new DoneListFragment();
        Bundle args = new Bundle();
        args.putInt(PREV_FRAGMENT, previouslyFragment);
        doneListFragment.setArguments(args);
        return doneListFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.donelist_recycler, container, false);
        v = mBinding.getRoot();
        res = getResources();
        setBackgroundColor(R.array.main, v);
        setActionBar();
        setBackgroundColor(R.array.bucket, mBinding.circularView);
        mBinding.setDoneClick(this);


        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        final NoteAdapter noteAdapter = new NoteAdapter();
        mBinding.recyclerView.setLayoutManager(mLinearLayoutManager);
        mBinding.recyclerView.setAdapter(noteAdapter);
        mBinding.recyclerView.setHasFixedSize(true);
        mBinding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int visibleItemCount = mLinearLayoutManager.getChildCount();
                int totalItemCount = mLinearLayoutManager.getItemCount();

                if ((visibleItemCount - totalItemCount == 0)) {
                    mBinding.calendarTask.show();
                    mBinding.addTask.show();
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0 && mBinding.calendarTask.isShown() && mBinding.addTask.isShown()) {
                    mBinding.calendarTask.hide();
                    mBinding.addTask.hide();
                } else if (dy < 0 && !mBinding.calendarTask.isShown() && !mBinding.addTask.isShown()) {
                    mBinding.calendarTask.show();
                    mBinding.addTask.show();

                }
            }
        });

        mDoneViewModel = ViewModelProviders.of(getActivity()).get(DoneViewModel.class);
        mBinding.setDoneVm(mDoneViewModel);
        mDoneViewModel.setPrevButton(getArguments().getInt(PREV_FRAGMENT, 1));

        //todo Как избежать использование Note ,а обращаться через VM? И нужно ли это? Тогда надо и адаптер RecyclerView менять?
        mDoneViewModel.getAllDoneNotes().observe(this, new Observer<List<Note>>() {
            @Override
            public void onChanged(List<Note> notes) {

                noteAdapter.submitList(notes);
            }
        });

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                mDoneViewModel.delete(noteAdapter.getNoteAt(viewHolder.getAdapterPosition()));
                Toast.makeText(getActivity(), res.getString(R.string.note_delete), Toast.LENGTH_SHORT).show();
            }

            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                GradientDrawable gdl = new GradientDrawable(
                        GradientDrawable.Orientation.RIGHT_LEFT, getResources().getIntArray(R.array.delete)
                );
                gdl.setCornerRadius(0f);
                gdl.setBounds(0, viewHolder.itemView.getTop(), (viewHolder.itemView.getRight()), viewHolder.itemView.getBottom());
                gdl.draw(c);

                new RecyclerViewSwipeDecorator.Builder(Objects.requireNonNull(getContext()), c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                        .addSwipeLeftLabel(getResources().getString(R.string.delete_note))
                        .setSwipeLeftLabelTypeface(getResources().getFont(R.font.exodus))
                        .setSwipeLeftLabelColor(getResources().getColor(R.color.nowhite))
                        .create()
                        .decorate();
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        }).attachToRecyclerView(mBinding.recyclerView);

        noteAdapter.setOnItemClickListener(new NoteAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(RecyclerViewModel note) {
                Intent intent = new Intent(getActivity(), AddEditNoteActivity.class);
                intent.putExtra(AddEditNoteActivity.EXTRA_ID, note.getNote().getId());
                getActivity().startActivityForResult(intent, REQUEST_EDIT_NOTE);

            }
        });
        return v;
    }

    private void setActionBar() {
        ((AppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(mBinding.layoutToolbarRecycler.toolbar);
        Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle(null);
        if ((mBinding.layoutToolbarRecycler.toolbar.getVisibility() == View.GONE)) {
            mBinding.layoutToolbarRecycler.toolbar.setVisibility(View.VISIBLE);
        }
        mBinding.layoutToolbarRecycler.toolbarTitle.setText(R.string.done_tasks);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void addTask() {
        startAnimation(mBinding.addTask);
        Intent intent = new Intent(getActivity(), AddEditNoteActivity.class);
        getActivity().startActivityForResult(intent, REQUEST_ADD_NOTE);
    }

    public void openCalendarFragment() {
        MenuViewModel menuViewModel = ViewModelProviders.of(getActivity()).get(MenuViewModel.class);
        menuViewModel.mFragment.setValue(2);
        startAnimation(mBinding.calendarTask);
        if (mDoneViewModel.mPrevButton.getValue() != null && mDoneViewModel.mPrevButton.getValue() == 1) {
            replaceFragment(new NoteListFragment());
        } else {
            replaceFragment(new CalendarListFragment());

        }

    }

    private void setBackgroundColor(int p, View circularView) {
        int[] background = getResources().getIntArray(p);
        GradientDrawable gd = new GradientDrawable(
                GradientDrawable.Orientation.TR_BL,
                background);
        gd.setCornerRadius(0f);
        circularView.setBackground(gd);
    }

    private void replaceFragment(Fragment fragmentToReplace) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.beginTransaction()
                .setCustomAnimations(R.animator.fade_in, R.animator.fade_out)
                .replace(R.id.fragment_container, fragmentToReplace)
                .commit();
    }

    private void startAnimation(FloatingActionButton fab) {
        int centerX = (fab.getLeft() + fab.getRight()) / 2;
        int centerY = (fab.getTop() + fab.getBottom()) / 2;
        float radius = Math.max(mBinding.circularView.getWidth(), mBinding.circularView.getHeight() * 2.0f);
        if (mBinding.circularView.getVisibility() == View.INVISIBLE) {
            mBinding.circularView.setVisibility(View.VISIBLE);
            ViewAnimationUtils.createCircularReveal(mBinding.circularView, centerX, centerY, 0, radius)
                    .start();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mBinding.circularView.getVisibility() == View.VISIBLE) {
            mBinding.circularView.setVisibility(View.INVISIBLE);
        }
    }

}
