package com.sheepApps.android.iamaslov.todo.view;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.sheepApps.android.iamaslov.todo.R;
import com.sheepApps.android.iamaslov.todo.databinding.ActivityAddEditNoteBinding;
import com.sheepApps.android.iamaslov.todo.view.pickerfragments.DatePickerFragment;
import com.sheepApps.android.iamaslov.todo.view.pickerfragments.TimePickerFragment;
import com.sheepApps.android.iamaslov.todo.viewmodel.AddEditViewModel;

import java.util.Calendar;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;


public class AddEditNoteActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    private static final String TAG = "AddEditNoteActivity";
    public static final String EXTRA_ID = " com.sheepApps.android.iamaslov.TODO.view.ID";
    public static final String BUNDLE_DATE_OF_TASK = " com.sheepApps.android.iamaslov.TODO.view.BUNDLE_DATE_OF_TASK";
    public static final String BUNDLE_TIME_OF_TASK = " com.sheepApps.android.iamaslov.TODO.view.BUNDLE_TIME_OF_TASK";
    private Resources res;
    private CircleImageView mOldBorderImage;
    private int mTaskIcon = 1;
    private int[] mColorsForGradient = {};

    ActivityAddEditNoteBinding mBinding;
    private AddEditViewModel mAddEditViewModel;
    //Todo
    private String mTitle = "";
    private String mDescription = "";
    private String mDateOfTask = "";
    private String mTimeOfTask = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_edit_note);
        mAddEditViewModel = ViewModelProviders.of(this).get(AddEditViewModel.class);
        mBinding.setAddeditvm(mAddEditViewModel);
        mBinding.setLifecycleOwner(this);
        mBinding.setOnClick(this);


        setSupportActionBar(mBinding.layoutToolbar.toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(null);
        setActionBarTitle();

        res = getResources();


        mBinding.noteDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogFragment datePicker = new DatePickerFragment();
                Bundle bundle = new Bundle();
                bundle.putString(BUNDLE_DATE_OF_TASK, getDateText());
                datePicker.setArguments(bundle);
                datePicker.show(getSupportFragmentManager(), "datePicker");

            }
        });


        mBinding.noteTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment timePicker = new TimePickerFragment();
                Bundle bundle = new Bundle();
                bundle.putString(BUNDLE_TIME_OF_TASK, getTimeText());
                timePicker.setArguments(bundle);
                timePicker.show(getSupportFragmentManager(), "timePicker");
            }
        });


        Intent intent = getIntent();

        if (intent.hasExtra(EXTRA_ID)) {
            mAddEditViewModel.setNoteAtributesForEditTask(intent.getIntExtra(EXTRA_ID, -1));
            setBackgroundColor(mAddEditViewModel.gradient.getValue());
            setIconTask(mAddEditViewModel.icon.getValue());
            setEditImageBorder(mAddEditViewModel.icon.getValue());

        } else {
            getAttributes();
            mColorsForGradient = res.getIntArray(R.array.bucket);
            setBackgroundColor(mColorsForGradient);

            if (mAddEditViewModel.icon.getValue() == null){
                setIconTask(1);
                setEditImageBorder(1);
            }else{
                setIconTask(mAddEditViewModel.icon.getValue());
                setEditImageBorder(mAddEditViewModel.icon.getValue());
            }

        }

        mBinding.setAddeditvm(mAddEditViewModel);

    }



    private void setActionBarTitle() {

        Intent intent = getIntent();
        if (intent.hasExtra(EXTRA_ID)) {
            mBinding.layoutToolbar.toolbarTitle.setText(R.string.edit_todo);
        } else {
            mBinding.layoutToolbar.toolbarTitle.setText(R.string.add_todo);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.save_note_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.save_note) {
            saveNote();
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    private void saveNote() {
        getAttributes();
        mAddEditViewModel.setNoteAtributes(mTitle, mDescription, mDateOfTask, mTimeOfTask,mColorsForGradient,mTaskIcon);
        Intent data = new Intent();

        int id = getIntent().getIntExtra(EXTRA_ID, -1);
        if (id != -1)
            data.putExtra(EXTRA_ID, id);
        mAddEditViewModel.onSaveClick(id);
        setResult(RESULT_OK, data);
        finish();
    }

    private void getAttributes() {
        mTitle = mBinding.editTitle.getText().toString();
        mDescription = mBinding.editDescription.getText().toString();
        mDateOfTask = mBinding.noteDate.getText().toString();
        mTimeOfTask = mBinding.noteTime.getText().toString();

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        String currentDateString = (String) DateFormat.format(getResources().getString(R.string.DF), (c.getTime()));
        mAddEditViewModel.date.setValue(currentDateString);

    }

    public String getDateText() {
        return mBinding.noteDate.getText().toString();
    }

    public String getTimeText() {
        return mBinding.noteTime.getText().toString();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        String currentDateString = (String) DateFormat.format(getResources().getString(R.string.TF), (calendar.getTime()));
        mAddEditViewModel.time.setValue(currentDateString);

    }

    private void setBackgroundColor(int[] colorsForGradient) {
        GradientDrawable gd = new GradientDrawable(
                GradientDrawable.Orientation.TR_BL,
                colorsForGradient);
        gd.setCornerRadius(0f);
        View view = this.getWindow().getDecorView();
        view.setBackground(gd);
    }

    public void setIconTask(int imageNumber) {
        mTaskIcon = imageNumber;
        mAddEditViewModel.icon.setValue(imageNumber);
        switch (mTaskIcon) {
            case 1:
                setImageBorder(mBinding.image1);
                mColorsForGradient = res.getIntArray(R.array.bucket);
                break;
            case 2:
                setImageBorder(mBinding.image2);
                mColorsForGradient = res.getIntArray(R.array.phone);
                break;
            case 3:

                setImageBorder(mBinding.image3);
                mColorsForGradient = res.getIntArray(R.array.coffee);
                break;
            case 4:
                setImageBorder(mBinding.image4);
                mColorsForGradient = res.getIntArray(R.array.medicine);
                break;
            case 5:
                setImageBorder(mBinding.image5);
                mColorsForGradient = res.getIntArray(R.array.workout);
                break;
            case 6:
                setImageBorder(mBinding.image6);
                mColorsForGradient = res.getIntArray(R.array.star);
                break;
            case 7:
                setImageBorder(mBinding.image7);
                mColorsForGradient = res.getIntArray(R.array.glasses);
                break;
            case 8:
                setImageBorder(mBinding.image8);
                mColorsForGradient = res.getIntArray(R.array.day_night);
                break;
            case 9:
                setImageBorder(mBinding.image9);
                mColorsForGradient = res.getIntArray(R.array.geo);
                break;
            case 10:
                setImageBorder(mBinding.image10);
                mColorsForGradient = res.getIntArray(R.array.planner);
                break;
        }
    }

    public void setImageBorder(CircleImageView image1) {
        if (mOldBorderImage == null) {
            mOldBorderImage = mBinding.image1;
        }
        mOldBorderImage.setBorderColor(res.getColor(R.color.black));
        image1.setBorderColor(res.getColor(R.color.nowhite));
        mOldBorderImage = image1;
    }

    private void setEditImageBorder(int intExtra) {
        switch (intExtra) {
            case 1:
                setImageBorder(mBinding.image1);
                break;
            case 2:
                setImageBorder(mBinding.image2);
                break;
            case 3:
                setImageBorder(mBinding.image3);
                break;
            case 4:
                setImageBorder(mBinding.image4);
                break;
            case 5:
                setImageBorder(mBinding.image5);
                break;
            case 6:
                setImageBorder(mBinding.image6);
                break;
            case 7:
                setImageBorder(mBinding.image7);
                break;
            case 8:
                setImageBorder(mBinding.image8);
                break;
            case 9:
                setImageBorder(mBinding.image9);
                break;
            case 10:
                setImageBorder(mBinding.image10);
                break;
        }
    }

}
