package com.sheepApps.android.iamaslov.todo.model;

import android.content.Context;
import android.os.AsyncTask;


import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {Note.class}, version = 7, exportSchema = false)
@TypeConverters({IntConverter.class})
public abstract class NoteDatabase extends RoomDatabase {
    public abstract NoteDao mNoteDao();


    private static NoteDatabase instance;

    public static synchronized NoteDatabase getInstance(Context context) {

        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    NoteDatabase.class, "TODO_database")
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .addCallback(sCallback)
                    .build();

        }
        return instance;
    }

    private static RoomDatabase.Callback sCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private final NoteDao mNoteDao;

        public PopulateDbAsyncTask(NoteDatabase db) {
            mNoteDao = db.mNoteDao();
        }

        @Override
        protected Void doInBackground(final Void... voids) {
            String array1 = "-15547671,-3902995,-41374";
            String array2 = "-16760470,-8806644";
            String array3 = "-27534,-888676";
            String array4 = "-16746095,-8847402";
            String array5 = "-14415050,-8453889,-65281,-135175";
            String array6 = "-14238149,-9866252";
            String array7 = "-65383,-11980224";
            String array8 = "-217019,-189155,-14415050";
            String array9 = "-7929856,-14473946";
            String array10 = "-16777216,-12369085";

            int[] colors1 = IntConverter.convertToInt(array1);
            int[] colors2 = IntConverter.convertToInt(array2);
            int[] colors3 = IntConverter.convertToInt(array3);
            int[] colors4 = IntConverter.convertToInt(array4);
            int[] colors5 = IntConverter.convertToInt(array5);
            int[] colors6 = IntConverter.convertToInt(array6);
            int[] colors7 = IntConverter.convertToInt(array7);
            int[] colors8 = IntConverter.convertToInt(array8);
            int[] colors9 = IntConverter.convertToInt(array9);
            int[] colors10 = IntConverter.convertToInt(array10);
            mNoteDao.insert(new Note("Title1","Description1","Mon, 15, Jul,2019","04:33",colors1,1));
            mNoteDao.insert(new Note("Title2","Description2","Tue, 16, Jul,2019","06:32",colors3,3));
            mNoteDao.insert(new Note("Title3","Description3","Wed, 17, Jul,2019","02:31",colors5,5));
            mNoteDao.insert(new Note("Title4","Description4","Thu, 18, Jul,2019","11:22",colors2,2));
            mNoteDao.insert(new Note("Title5","Description5","Fri, 19, Jul,2019","12:45",colors5,5));
            mNoteDao.insert(new Note("Title6","Description6","Sat, 20, Jul,2019","07:33",colors7,7));
            mNoteDao.insert(new Note("Title7","Description7","Sun, 21, Jul,2019","08:16",colors9,9));
            mNoteDao.insert(new Note("Title8","Description1","Mon, 22, Jul,2019","02:26",colors6,6));
            mNoteDao.insert(new Note("Title9","Description2","Mon, 15, Jul,2019","03:26",colors10,10));
            mNoteDao.insert(new Note("Title10","Description3","Mon, 15, Jul,2019","07:54",colors3,3));
            mNoteDao.insert(new Note("Title11","Description4","Tue, 16, Jul,2019","03:53",colors4,4));
            mNoteDao.insert(new Note("Title12","Description5","Wed, 17, Jul,2019","06:22",colors8,8));
            mNoteDao.insert(new Note("Title13","Description6","Thu, 18, Jul,2019","08:11",colors9,9));
            mNoteDao.insert(new Note("Title14","Description7","Fri, 19, Jul,2019","05:31",colors1,1));
            mNoteDao.insert(new Note("Title15","Description1","Sat, 20, Jul,2019","04:33",colors1,1));
            mNoteDao.insert(new Note("Title16","Description1","Sun, 21, Jul,2019","04:32",colors1,1));
            mNoteDao.insert(new Note("Title17","Description2","Mon, 15, Jul,2019","06:31",colors3,3));
            mNoteDao.insert(new Note("Title18","Description3","Tue, 16, Jul,2019","02:34",colors5,5));
            mNoteDao.insert(new Note("Title19","Description4","Wed, 17, Jul,2019","11:27",colors2,2));
            mNoteDao.insert(new Note("Title20","Description5","Thu, 18, Jul,2019","12:48",colors5,5));
            mNoteDao.insert(new Note("Title21","Description1","Fri, 19, Jul,2019","04:33",colors1,1));
            mNoteDao.insert(new Note("Title22","Description1","Sat, 20, Jul,2019","04:33",colors1,1));


            return null;

        }
    }


}
