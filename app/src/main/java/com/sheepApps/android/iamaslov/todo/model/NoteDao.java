package com.sheepApps.android.iamaslov.todo.model;


import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface NoteDao {

    @Query("Select * from note where mDoneIndex = 0 ")
    LiveData<List<Note>> getAllNotes();

    @Query("Select * from note where mDoneIndex = 1 ")
    LiveData<List<Note>> getAllDoneNotes();

    @Query("Select * from note where mDoneIndex = 0 and mDate LIKE :mDay ORDER BY mTime ")
    LiveData<List<Note>> getCalendarNotes(String mDay);

    @Query("Select * from note where mId =:id")
    Note getNote(int id);

    @Delete
    void delete(Note note);

    @Query("Delete from note where mDoneIndex != 1 ")
    void deleteAll();

    @Query("Delete from note where mDoneIndex = 1 ")
    void deleteAllDone();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Note note);

    @Update
    void update(Note note);

    @Update
    void updateDone(Note note);


}
