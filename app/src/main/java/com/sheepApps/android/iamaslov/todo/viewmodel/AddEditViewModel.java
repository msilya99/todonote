package com.sheepApps.android.iamaslov.todo.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.sheepApps.android.iamaslov.todo.model.Note;
import com.sheepApps.android.iamaslov.todo.repository.NoteRepository;

public class AddEditViewModel extends AndroidViewModel {
    private Note mNote;
    private NoteRepository mNoteRepository;
    public MutableLiveData<String> title = new MutableLiveData<>();
    public MutableLiveData<String> description = new MutableLiveData<>();
    public MutableLiveData<String> date = new MutableLiveData<>();
    public MutableLiveData<String> time = new MutableLiveData<>();
    public MutableLiveData<int[]> gradient = new MutableLiveData<>();
    public MutableLiveData<Integer> icon = new MutableLiveData<>();

    public AddEditViewModel(@NonNull Application application) {
        super(application);
        mNoteRepository = new NoteRepository(application);
    }


    public void onSaveClick(int id) {
        if (id == -1) {
            mNote = new Note(title.getValue(),description.getValue(),date.getValue(),time.getValue(),gradient.getValue(),icon.getValue());
            mNote.setDoneIndex(0);
            insert(mNote);
        } else {
            mNote =  getNote(id);
            mNote.setTitle(title.getValue());
            mNote.setDescription(description.getValue());
            mNote.setDate(date.getValue());
            mNote.setTime(time.getValue());
            mNote.setGradient(gradient.getValue());
            mNote.setIcon(icon.getValue());
            update(mNote);
        }

    }

    public Note getNote(int id) {
        return mNote = mNoteRepository.getNote(id);
    }


    public void insert(Note note) {
        mNoteRepository.insert(note);
    }

    public void update(Note note) {
        mNoteRepository.update(note);
    }


    public void setNoteAtributes(String mTitle,String mDescription,String mDate,String mTime,int[] mGradient,int mIcon) {
        title.setValue(mTitle);
        description.setValue(mDescription);
        date.setValue(mDate);
        time.setValue(mTime);
        gradient.setValue(mGradient);
        icon.setValue(mIcon);
    }

    public void setNoteAtributesForEditTask(int id) {
        Note note = getNote(id);
        title.setValue(note.getTitle());
        description.setValue(note.getDescription());
        if (date.getValue()==null){
            date.setValue(note.getDate());
        }
        if (time.getValue()==null){
            time.setValue(note.getTime());
        }
        if (icon.getValue()==null){
            icon.setValue(note.getIcon());
        }
        if (gradient.getValue()==null){
            gradient.setValue(note.getGradient());
        }


    }



}
