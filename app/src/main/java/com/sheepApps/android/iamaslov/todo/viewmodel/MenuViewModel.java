package com.sheepApps.android.iamaslov.todo.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

public class MenuViewModel extends AndroidViewModel {
    public MutableLiveData<Integer> mFragment = new MutableLiveData<>();

    public MenuViewModel(@NonNull Application application) {
        super(application);
    }

    public void setFragment(MutableLiveData<Integer> fragment) {
        mFragment = fragment;
    }
}
