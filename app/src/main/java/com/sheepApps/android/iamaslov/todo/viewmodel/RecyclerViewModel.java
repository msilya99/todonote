package com.sheepApps.android.iamaslov.todo.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.sheepApps.android.iamaslov.todo.model.Note;
import com.sheepApps.android.iamaslov.todo.repository.NoteRepository;

import java.util.List;

public class RecyclerViewModel extends ViewModel {
    private Note mNote;
    private List<Note> mNotes;
    private NoteRepository mNoteRepository;
    public MutableLiveData<String> title = new MutableLiveData<>();
    public MutableLiveData<String> date = new MutableLiveData<>();
    public MutableLiveData<String> time = new MutableLiveData<>();
    public MutableLiveData<Integer> icon = new MutableLiveData<>();


    public Note getNote(int id) {
        return mNote = mNoteRepository.getNote(id);
    }


    public RecyclerViewModel() {
        mNote = new Note();
    }

    public RecyclerViewModel(Note note) {
        mNote = note;
        title.setValue(note.getTitle());
        date.setValue(note.getDate());
        time.setValue(note.getTime());
        icon.setValue(note.getIcon());
    }

    public void setNote(Note note) {
        mNote = note;
    }

    public Note getNote() {
        return mNote;
    }

    public List<Note> getAllNotes(){
        return mNotes;
    }


}
