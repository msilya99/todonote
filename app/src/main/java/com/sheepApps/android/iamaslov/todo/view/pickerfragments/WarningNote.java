package com.sheepApps.android.iamaslov.todo.view.pickerfragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.sheepApps.android.iamaslov.todo.R;
import com.sheepApps.android.iamaslov.todo.viewmodel.DoneViewModel;
import com.sheepApps.android.iamaslov.todo.viewmodel.MenuViewModel;
import com.sheepApps.android.iamaslov.todo.viewmodel.NoteViewModel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.lifecycle.ViewModelProviders;

public class WarningNote extends AppCompatDialogFragment {

    private Context context;
    private NoteViewModel mNoteViewModel;
    private DoneViewModel mDoneViewModel;
    private MenuViewModel mMenuViewModel;
    public static final String TAG_DIALOG_NOTE_DELETE_ALL = "note_delete_all";


    public static WarningNote newInstance() {
        WarningNote fragment = new WarningNote();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mNoteViewModel = ViewModelProviders.of(getActivity()).get(NoteViewModel.class);
        mDoneViewModel = ViewModelProviders.of(getActivity()).get(DoneViewModel.class);
        mMenuViewModel = ViewModelProviders.of(getActivity()).get(MenuViewModel.class);


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        if (mMenuViewModel.mFragment.getValue()!= null && mMenuViewModel.mFragment.getValue() == 1){
            View view = getActivity().getLayoutInflater().inflate(R.layout.warning_note_delete, null);
            TextView warningTaskTextView = view.findViewById(R.id.warning_view);
            warningTaskTextView.setText(R.string.note_delete_warning);

            alertDialogBuilder.setView(view)
                    .setTitle(getString(R.string.warning_done))
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mDoneViewModel.deleteAllDone();
                            Toast.makeText(getActivity(), getString(R.string.toast_delete_all_done), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
        }else{
            View view = getActivity().getLayoutInflater().inflate(R.layout.warning_note_delete, null);
            TextView warningTaskTextView = view.findViewById(R.id.warning_view);
            warningTaskTextView.setText(R.string.note_delete_warning);

            alertDialogBuilder.setView(view)
                    .setTitle(getString(R.string.warning_all))
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mNoteViewModel.deleteAll();
                            Toast.makeText(getActivity(), getString(R.string.toast_delete_all), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
        }


        return alertDialogBuilder.create();
    }


}

