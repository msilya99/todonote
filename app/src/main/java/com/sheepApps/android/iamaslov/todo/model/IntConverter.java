package com.sheepApps.android.iamaslov.todo.model;



import java.util.Arrays;

import androidx.room.TypeConverter;

public class IntConverter {
    @TypeConverter
    public static String convertToString(int[] number) {
        String str[] = new String[number.length];
        for (int i = 0; i < str.length; i++) {
            str[i] = String.valueOf(number[i]);
        }
        String stringGradient = Arrays.toString(str).replaceAll("\\[|\\]|\\s", "");
            return stringGradient;
    }

    @TypeConverter
    public static int[] convertToInt(String str) {
        String[] integerStrings = str.split(",");
        int[] integers = new int[integerStrings.length];
        for (int i = 0; i < integers.length; i++) {
            integers[i] = Integer.parseInt(integerStrings[i]);
        }
            return integers;
    }
}
