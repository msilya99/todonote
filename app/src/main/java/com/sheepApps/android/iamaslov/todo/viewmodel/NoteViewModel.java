package com.sheepApps.android.iamaslov.todo.viewmodel;

import android.app.Application;

import com.sheepApps.android.iamaslov.todo.model.Note;
import com.sheepApps.android.iamaslov.todo.repository.NoteRepository;

import java.util.List;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class NoteViewModel extends AndroidViewModel  {

    private NoteRepository mNoteRepository;
    private LiveData<List<Note>> mAllNotes;
    private Note mNote;

    public NoteViewModel(Application application) {
        super(application);
        mNoteRepository = new NoteRepository(application);
        mAllNotes = mNoteRepository.getAllNotes();
    }

    public LiveData<List<Note>> getAllNotes() {
        return mAllNotes;
    }


    public Note getNote(int id){
        return mNote = mNoteRepository.getNote(id);
    }

    public void updateDone(Note note) {
        mNoteRepository.updateDone(note);
    }

    public void delete(Note note) {
        mNoteRepository.delete(note);
    }

    public void deleteAll() {
        mNoteRepository.deleteAllNotes();
    }

}
