package com.sheepApps.android.iamaslov.todo.view.pickerfragments;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.TimePickerDialog;

import android.os.Bundle;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.sheepApps.android.iamaslov.todo.R;

import static com.sheepApps.android.iamaslov.todo.view.AddEditNoteActivity.BUNDLE_TIME_OF_TASK;

public class TimePickerFragment extends DialogFragment {
    private static final String TAG = "TimePickerFragment";

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            String timeOfTask = bundle.getString(BUNDLE_TIME_OF_TASK, "");
            @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat(getResources().getString(R.string.TF));
            try {
                Date stringToDate = formatter.parse(timeOfTask);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(stringToDate);
                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);

                return new TimePickerDialog(getActivity(),R.style.DialogThemeDateTime,
                        (TimePickerDialog.OnTimeSetListener) getActivity(), hour, minute,
                        true);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        return new TimePickerDialog(getActivity(),R.style.DialogThemeDateTime,
                (TimePickerDialog.OnTimeSetListener) getActivity(), hour, minute,
                true);
    }
}
