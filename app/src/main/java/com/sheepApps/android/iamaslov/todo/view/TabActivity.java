package com.sheepApps.android.iamaslov.todo.view;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;

import com.sheepApps.android.iamaslov.todo.R;
import com.sheepApps.android.iamaslov.todo.view.pickerfragments.WarningNote;
import com.sheepApps.android.iamaslov.todo.viewmodel.MenuViewModel;
import com.sheepApps.android.iamaslov.todo.viewmodel.NoteViewModel;

import static com.sheepApps.android.iamaslov.todo.view.pickerfragments.WarningNote.TAG_DIALOG_NOTE_DELETE_ALL;


public class TabActivity extends SingleFragmentActivity {
    private static final String TAG = "TabActivity";
    public static final int REQUEST_ADD_NOTE = 1;
    public static final int REQUEST_EDIT_NOTE = 2;
    private Resources res;
    private MenuViewModel mMenuViewModel;

    @Override
    protected Fragment createFragment() {
        return new NoteListFragment();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        res = getResources();
        mMenuViewModel = ViewModelProviders.of(this).get(MenuViewModel.class);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_ADD_NOTE && resultCode == RESULT_OK) {
            Toast.makeText(this, res.getString(R.string.note_saved), Toast.LENGTH_SHORT).show();
        } else if (requestCode == REQUEST_EDIT_NOTE && resultCode == RESULT_OK) {
            assert data != null;
            int id = data.getIntExtra(AddEditNoteActivity.EXTRA_ID, -1);
            if (id == -1) {
                Toast.makeText(this, res.getString(R.string.note_not_update), Toast.LENGTH_SHORT).show();
                return;
            }
            Toast.makeText(this, res.getString(R.string.note_update), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.delete_all, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.delete_all:
                    FragmentManager manager = this.getSupportFragmentManager();
                    DialogFragment dialogFragment = WarningNote.newInstance();
                    dialogFragment.show(manager, TAG_DIALOG_NOTE_DELETE_ALL);

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
