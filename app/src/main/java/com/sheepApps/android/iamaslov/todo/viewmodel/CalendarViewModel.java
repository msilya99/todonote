package com.sheepApps.android.iamaslov.todo.viewmodel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.sheepApps.android.iamaslov.todo.model.Note;
import com.sheepApps.android.iamaslov.todo.repository.NoteRepository;

import java.util.List;

public class CalendarViewModel extends AndroidViewModel  {
    private Note mNote;
    private NoteRepository mNoteRepository;

    public CalendarViewModel(Application application) {
        super(application);
        mNoteRepository = new NoteRepository(application);
    }

    public LiveData<List<Note>> getCalendarNotes(String day) {
        LiveData<List<Note>> calendarNotes;
        return calendarNotes = mNoteRepository.getCalendarNotes(day);
    }

    public Note getNote(int id){
        return mNote = mNoteRepository.getNote(id);
    }

    public void updateDone(Note note) {
        mNoteRepository.updateDone(note);
    }

    public void delete(Note note) {
        mNoteRepository.delete(note);
    }


}
