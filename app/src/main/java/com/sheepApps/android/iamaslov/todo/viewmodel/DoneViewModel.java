package com.sheepApps.android.iamaslov.todo.viewmodel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.sheepApps.android.iamaslov.todo.model.Note;
import com.sheepApps.android.iamaslov.todo.repository.NoteRepository;

import java.util.List;

public class DoneViewModel extends AndroidViewModel  {

    private NoteRepository mNoteRepository;
    private LiveData<List<Note>> mAllDoneNotes;
    private Note mNote;
    public MutableLiveData<Integer> mPrevButton = new MutableLiveData<>();

    public DoneViewModel(Application application) {
        super(application);
        mNoteRepository = new NoteRepository(application);
        mAllDoneNotes = mNoteRepository.getAllDoneNotes();
    }
    public LiveData<List<Note>> getAllDoneNotes() {
        return mAllDoneNotes;
    }

    public Note getNote(int id){
        return mNote = mNoteRepository.getNote(id);
    }

    public void delete(Note note) {
        mNoteRepository.delete(note);
    }

    public void deleteAllDone() {
        mNoteRepository.deleteAllDone();
    }

    public void setPrevButton(int prevButton){
        mPrevButton.setValue(prevButton);
    }

}
