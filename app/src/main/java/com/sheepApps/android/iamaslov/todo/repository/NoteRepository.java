package com.sheepApps.android.iamaslov.todo.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.sheepApps.android.iamaslov.todo.model.Note;
import com.sheepApps.android.iamaslov.todo.model.NoteDao;
import com.sheepApps.android.iamaslov.todo.model.NoteDatabase;

import java.util.List;

import androidx.lifecycle.LiveData;


public class NoteRepository {
    private NoteDao mNoteDao;
    private LiveData<List<Note>> mAllNotes;
    private LiveData<List<Note>> mAllDoneNotes;
    private LiveData<List<Note>> mCalendarNotes;
    private Note mNote;


    public NoteRepository(Application application) {
        NoteDatabase database = NoteDatabase.getInstance(application);
        mNoteDao = database.mNoteDao();
        mAllNotes = mNoteDao.getAllNotes();
        mAllDoneNotes = mNoteDao.getAllDoneNotes();
    }


    public LiveData<List<Note>> getAllNotes() {
        return mAllNotes;
    }

    public LiveData<List<Note>> getAllDoneNotes() {
        return mAllDoneNotes;
    }



    public LiveData<List<Note>> getCalendarNotes(String day) {
      return mCalendarNotes = mNoteDao.getCalendarNotes(day);
    }

    public Note getNote(int id){
        return  mNote = mNoteDao.getNote(id);
    }



    public void insert(Note note) {
        new InsertAsyncTask(mNoteDao).execute(note);
    }

    private static class InsertAsyncTask extends AsyncTask<Note, Void, Void> {
        private NoteDao mAsyncTaskNoteDao;

        public InsertAsyncTask(NoteDao noteDao) {
            mAsyncTaskNoteDao = noteDao;
        }

        @Override
        protected Void doInBackground(Note... notes) {
            mAsyncTaskNoteDao.insert(notes[0]);
            return null;
        }
    }

    public void update(Note note) {
        new UpdateAsyncTask(mNoteDao).execute(note);

    }

    private static class UpdateAsyncTask extends AsyncTask<Note, Void, Void> {
        private NoteDao mAsyncTaskNoteDao;

        public UpdateAsyncTask(NoteDao noteDao) {
            mAsyncTaskNoteDao = noteDao;
        }

        @Override
        protected Void doInBackground(Note... notes) {
            mAsyncTaskNoteDao.update(notes[0]);
            return null;
        }
    }

    public void updateDone(Note note) {
        new UpdateDoneAsyncTask(mNoteDao).execute(note);

    }

    private static class UpdateDoneAsyncTask extends AsyncTask<Note, Void, Void> {
        private NoteDao mAsyncTaskNoteDao;

        public UpdateDoneAsyncTask(NoteDao noteDao) {
            mAsyncTaskNoteDao = noteDao;
        }

        @Override
        protected Void doInBackground(Note... notes) {
            notes[0].setDoneIndex(1);
            mAsyncTaskNoteDao.updateDone(notes[0]);
            return null;
        }
    }


    public void delete(Note note) {
        new DeleteAsyncTask(mNoteDao).execute(note);
    }

    private static class DeleteAsyncTask extends AsyncTask<Note, Void, Void> {
        private NoteDao mAsyncTaskNoteDao;

        public DeleteAsyncTask(NoteDao noteDao) {
            mAsyncTaskNoteDao = noteDao;
        }

        @Override
        protected Void doInBackground(Note... notes) {
            mAsyncTaskNoteDao.delete(notes[0]);
            return null;
        }
    }

    public void deleteAllNotes() {
        new DeleteAllNotesAsyncTask(mNoteDao).execute();
    }

    private static class DeleteAllNotesAsyncTask extends AsyncTask<Void, Void, Void> {
        private NoteDao mAsyncTaskNoteDao;

        public DeleteAllNotesAsyncTask(NoteDao noteDao) {
            mAsyncTaskNoteDao = noteDao;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskNoteDao.deleteAll();
            return null;
        }
    }

    public void deleteAllDone() {
        new DeleteAllDoneAsyncTask(mNoteDao).execute();
    }

    private static class DeleteAllDoneAsyncTask extends AsyncTask<Void, Void, Void> {
        private NoteDao mAsyncTaskNoteDao;

        public DeleteAllDoneAsyncTask(NoteDao noteDao) {
            mAsyncTaskNoteDao = noteDao;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskNoteDao.deleteAllDone();
            return null;
        }
    }

}

