package com.sheepApps.android.iamaslov.todo.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.sheepApps.android.iamaslov.todo.R;
import com.sheepApps.android.iamaslov.todo.databinding.NoteItemBinding;
import com.sheepApps.android.iamaslov.todo.model.Note;
import com.sheepApps.android.iamaslov.todo.viewmodel.RecyclerViewModel;

public class NoteAdapter extends ListAdapter<Note, NoteAdapter.NoteHolder> {
    private OnItemClickListener mListener;
    private Context mContext;
    private RecyclerViewModel mRecyclerViewModel;


    public NoteAdapter() {
        super(DIFF_CALLBACK);
    }


    private static final DiffUtil.ItemCallback<Note> DIFF_CALLBACK = new DiffUtil.ItemCallback<Note>() {
        @Override
        public boolean areItemsTheSame(@NonNull Note oldItem, @NonNull Note newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Note oldItem, @NonNull Note newItem) {
            return oldItem.getTitle().equals(newItem.getTitle()) &&
                    oldItem.getDescription().equals(newItem.getDescription()) &&
                    oldItem.getDate().equals(newItem.getDate()) &&
                    oldItem.getTime().equals(newItem.getTime()) &&
                    oldItem.getIcon() == (newItem.getIcon());
        }
    };

    @NonNull
    @Override
    public NoteHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        mContext = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        NoteItemBinding mBinding = DataBindingUtil.inflate(inflater,R.layout.note_item,viewGroup, false);
        return new NoteHolder(mBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull NoteHolder noteHolder, int i) {
        mRecyclerViewModel = new RecyclerViewModel(getItem(i));
        noteHolder.mBinding.setNote(mRecyclerViewModel);
        setTaskImage(noteHolder);
        noteHolder.mBinding.cardView.setAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_scale_animation));
    }


    private void setTaskImage(@NonNull NoteHolder noteHolder) {
        if (mRecyclerViewModel.icon.getValue() != null)
        switch (mRecyclerViewModel.icon.getValue()) {
            case 1:
                noteHolder.mBinding.taskImage.setImageResource(R.drawable.shopping);
                break;
            case 2:
                noteHolder.mBinding.taskImage.setImageResource(R.drawable.phone);
                break;
            case 3:
                noteHolder.mBinding.taskImage.setImageResource(R.drawable.coffee);
                break;
            case 4:
                noteHolder.mBinding.taskImage.setImageResource(R.drawable.medicine);
                break;
            case 5:
                noteHolder.mBinding.taskImage.setImageResource(R.drawable.workout);
                break;
            case 6:
                noteHolder.mBinding.taskImage.setImageResource(R.drawable.star);
                break;
            case 7:
                noteHolder.mBinding.taskImage.setImageResource(R.drawable.drinking);
                break;
            case 8:
                noteHolder.mBinding.taskImage.setImageResource(R.drawable.daynight);
                break;
            case 9:
                noteHolder.mBinding.taskImage.setImageResource(R.drawable.geo);
                break;
            case 10:
                noteHolder.mBinding.taskImage.setImageResource(R.drawable.planner);
                break;
        }
    }


    public Note getNoteAt(int position) {
        return getItem(position);
    }


    class NoteHolder extends RecyclerView.ViewHolder {
        private NoteItemBinding mBinding;

        public NoteHolder(@NonNull View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (mListener != null && position != RecyclerView.NO_POSITION) {
                        mRecyclerViewModel.setNote(getNoteAt(position));
                        mListener.onItemClick(mRecyclerViewModel);
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(RecyclerViewModel note);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }
}
