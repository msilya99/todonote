package com.sheepApps.android.iamaslov.todo.view;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;

import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sheepApps.android.iamaslov.todo.R;
import com.sheepApps.android.iamaslov.todo.databinding.CalendarRecyclerBinding;
import com.sheepApps.android.iamaslov.todo.databinding.RecyclerViewBinding;
import com.sheepApps.android.iamaslov.todo.model.Note;
import com.sheepApps.android.iamaslov.todo.viewmodel.CalendarViewModel;
import com.sheepApps.android.iamaslov.todo.viewmodel.MenuViewModel;
import com.sheepApps.android.iamaslov.todo.viewmodel.NoteViewModel;
import com.sheepApps.android.iamaslov.todo.viewmodel.RecyclerViewModel;
import com.shrikanthravi.collapsiblecalendarview.data.Day;
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator;

import static com.sheepApps.android.iamaslov.todo.view.TabActivity.REQUEST_ADD_NOTE;
import static com.sheepApps.android.iamaslov.todo.view.TabActivity.REQUEST_EDIT_NOTE;


public class CalendarListFragment extends Fragment {
    private static final String TAG = "CalendarListFragment";

    private CalendarViewModel mCalendarViewModel;
    private String mSelectedDay;
    private NoteAdapter noteAdapter;
    private View v;
    private Day dateToday;
    private int[] colors;


    private Resources res;
    private LinearLayoutManager mLinearLayoutManager;
    private CalendarRecyclerBinding mBinding;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.calendar_recycler, container, false);
        v = mBinding.getRoot();
        res = getResources();
        setBackgroundColor(R.array.main, v);
        setActionBar();
        getToday();
        setBackgroundColor(R.array.bucket, mBinding.circularView);
        mBinding.setCalendarClick(this);

        mLinearLayoutManager = new LinearLayoutManager(getActivity());

        mBinding.recyclerView.setLayoutManager(mLinearLayoutManager);
        mBinding.recyclerView.setHasFixedSize(true);
        recreateRecyclerView();
        mBinding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int visibleItemCount = mLinearLayoutManager.getChildCount();
                int totalItemCount = mLinearLayoutManager.getItemCount();

                if ((visibleItemCount - totalItemCount == 0)) {
                    mBinding.calendarTask.show();
                    mBinding.doneTask.show();
                    mBinding.addTask.show();
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0 && mBinding.calendarTask.isShown() && mBinding.doneTask.isShown() && mBinding.addTask.isShown()) {
                    mBinding.calendarTask.hide();
                    mBinding.doneTask.hide();
                    mBinding.addTask.hide();
                } else if (dy < 0 && !mBinding.calendarTask.isShown() && !mBinding.doneTask.isShown() && !mBinding.addTask.isShown()) {
                    mBinding.calendarTask.show();
                    mBinding.doneTask.show();
                    mBinding.addTask.show();
                }
            }
        });


        mBinding.layoutToolbarRecycler.calendarView.setVisibility(View.VISIBLE);
        mBinding.layoutToolbarRecycler.calendarView.select(dateToday);
        mBinding.layoutToolbarRecycler.calendarView.setCalendarListener(new CollapsibleCalendar.CalendarListener() {
            @Override
            public void onDaySelect() {
                Calendar calendar = Calendar.getInstance();
                Day day = mBinding.layoutToolbarRecycler.calendarView.getSelectedDay();
                calendar.set(day.getYear(), day.getMonth(), day.getDay());
                mSelectedDay = (String) DateFormat.format(getResources().getString(R.string.DF), calendar);
                recreateRecyclerView();
            }

            @Override
            public void onItemClick(View v) {

            }

            @Override
            public void onDataUpdate() {

            }

            @Override
            public void onMonthChange() {

            }

            @Override
            public void onWeekChange(int position) {

            }
        });

        return v;
    }

    private void setActionBar() {
        mBinding.layoutToolbarRecycler.toolbar.setVisibility(View.GONE);
    }


    private void recreateRecyclerView() {
        noteAdapter = new NoteAdapter();
        mBinding.recyclerView.setAdapter(noteAdapter);
        mCalendarViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(CalendarViewModel.class);

        //todo Как избежать использование Note ,а обращаться через VM? И нужно ли это? Тогда надо и адаптер RecyclerView менять?
        mCalendarViewModel.getCalendarNotes(mSelectedDay).observe(getActivity(), new Observer<List<Note>>() {
            @Override
            public void onChanged(List<Note> notes) {

                noteAdapter.submitList(notes);
            }
        });
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSelectedChanged(@Nullable RecyclerView.ViewHolder viewHolder, int actionState) {
                super.onSelectedChanged(viewHolder, actionState);
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    assert viewHolder != null;
                    colors = noteAdapter.getNoteAt(viewHolder.getAdapterPosition()).getGradient();
                }
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

                final int position = viewHolder.getAdapterPosition();
                if (direction == ItemTouchHelper.LEFT) {
                    mCalendarViewModel.delete(noteAdapter.getNoteAt(position));
                    Toast.makeText(getActivity(), res.getString(R.string.note_delete), Toast.LENGTH_SHORT).show();
                } else {
                    mCalendarViewModel.updateDone(noteAdapter.getNoteAt(position));
                    Toast.makeText(getActivity(), res.getString(R.string.note_done), Toast.LENGTH_SHORT).show();
                }

            }

            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                if (dX > 0) {
                    GradientDrawable gdr = new GradientDrawable(
                            GradientDrawable.Orientation.LEFT_RIGHT, colors
                    );
                    gdr.setCornerRadius(0f);
                    gdr.setBounds(0, viewHolder.itemView.getTop(), (viewHolder.itemView.getRight()), viewHolder.itemView.getBottom());
                    gdr.draw(c);

                    new RecyclerViewSwipeDecorator.Builder(Objects.requireNonNull(getContext()), c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                            .addSwipeRightLabel(getResources().getString(R.string.done_note))
                            .setSwipeRightLabelTypeface(getResources().getFont(R.font.exodus))
                            .setSwipeRightLabelColor(getResources().getColor(R.color.nowhite))
                            .create()
                            .decorate();
                } else {
                    GradientDrawable gdl = new GradientDrawable(
                            GradientDrawable.Orientation.RIGHT_LEFT, getResources().getIntArray(R.array.delete)
                    );
                    gdl.setCornerRadius(0f);
                    gdl.setBounds(0, viewHolder.itemView.getTop(), (viewHolder.itemView.getRight()), viewHolder.itemView.getBottom());
                    gdl.draw(c);

                    new RecyclerViewSwipeDecorator.Builder(Objects.requireNonNull(getContext()), c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                            .addSwipeLeftLabel(getResources().getString(R.string.delete_note))
                            .setSwipeLeftLabelTypeface(getResources().getFont(R.font.exodus))
                            .setSwipeLeftLabelColor(getResources().getColor(R.color.nowhite))
                            .create()
                            .decorate();
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);

            }
        }).attachToRecyclerView(mBinding.recyclerView);

        noteAdapter.setOnItemClickListener(new NoteAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(RecyclerViewModel note) {
                Intent intent = new Intent(getActivity(), AddEditNoteActivity.class);
                intent.putExtra(AddEditNoteActivity.EXTRA_ID, note.getNote().getId());
                getActivity().startActivityForResult(intent, REQUEST_EDIT_NOTE);

            }
        });

    }


    public void addTask() {
        startAnimation(mBinding.addTask);
        Intent intent = new Intent(getActivity(), AddEditNoteActivity.class);
        getActivity().startActivityForResult(intent, REQUEST_ADD_NOTE);
    }

    public void openCalendarFragment() {
        startAnimation(mBinding.calendarTask);
        replaceFragment(new NoteListFragment());

    }

    public void openDoneListFragment() {
        MenuViewModel menuViewModel = ViewModelProviders.of(getActivity()).get(MenuViewModel.class);
        menuViewModel.mFragment.setValue(1);
        startAnimation(mBinding.doneTask);
        replaceFragment(DoneListFragment.newInstance(2));
    }

    private void getToday() {
        Date d = new Date();
        String s = (String) DateFormat.format(getResources().getString(R.string.DF), d.getTime());
        mSelectedDay = s;
        Calendar selectedDayCalendar = Calendar.getInstance();
        int year = selectedDayCalendar.get(Calendar.YEAR);
        int month = selectedDayCalendar.get(Calendar.MONTH);
        int day = selectedDayCalendar.get(Calendar.DAY_OF_MONTH);
        dateToday = new Day(year, month, day);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private void setBackgroundColor(int p, View circularView) {
        int[] background = getResources().getIntArray(p);
        GradientDrawable gd = new GradientDrawable(
                GradientDrawable.Orientation.TR_BL,
                background);
        gd.setCornerRadius(0f);
        circularView.setBackground(gd);
    }


    private void replaceFragment(Fragment fragmentToReplace) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.beginTransaction()
                .setCustomAnimations(R.animator.fade_in, R.animator.fade_out)
                .replace(R.id.fragment_container, fragmentToReplace)
                .commit();
    }


    private void startAnimation(FloatingActionButton fab) {
        int centerX = (fab.getLeft() + fab.getRight()) / 2;
        int centerY = (fab.getTop() + fab.getBottom()) / 2;
        float radius = Math.max(mBinding.circularView.getWidth(), mBinding.circularView.getHeight() * 2.0f);
        if (mBinding.circularView.getVisibility() == View.INVISIBLE) {
            mBinding.circularView.setVisibility(View.VISIBLE);
            ViewAnimationUtils.createCircularReveal(mBinding.circularView, centerX, centerY, 0, radius)
                    .start();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mBinding.circularView.getVisibility() == View.VISIBLE) {
            mBinding.circularView.setVisibility(View.INVISIBLE);
        }
    }

}
