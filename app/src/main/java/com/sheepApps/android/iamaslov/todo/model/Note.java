package com.sheepApps.android.iamaslov.todo.model;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

@Entity(tableName = "note")
@TypeConverters({IntConverter.class})
public class Note extends BaseObservable {

    @PrimaryKey(autoGenerate = true)
    private int mId;
    private String mTitle;
    private String mDescription;
    private String mDate;
    private String mTime;
    private int[] mGradient;
    private int mIcon;
    private int mDoneIndex;


    @Ignore
    public Note() {
    }

    public Note(String title, String description, String date, String time, int[] gradient, int icon) {
        mTitle = title;
        mDescription = description;
        mDate = date;
        mTime = time;
        mGradient = gradient;
        mIcon = icon;
    }

    @Bindable
    public int[] getGradient() {
        return mGradient;
    }

    @Bindable
    public String getDate() {
        return mDate;
    }

    @Bindable
    public String getTime() {
        return mTime;
    }


    @Bindable
    public int getDoneIndex() {
        return mDoneIndex;
    }

    @Bindable
    public int getId() {
        return mId;
    }

    @Bindable
    public String getTitle() {
        return mTitle;
    }

    @Bindable
    public String getDescription() {
        return mDescription;
    }

    @Bindable
    public int getIcon() {
        return mIcon;
    }

    public void setId(int id) {
        mId = id;
        notifyPropertyChanged(BR._all);
    }

    public void setTitle(String title) {
        mTitle = title;
        notifyPropertyChanged(BR._all);
    }

    public void setDescription(String description) {
        mDescription = description;
        notifyPropertyChanged(BR._all);
    }

    public void setDate(String date) {
        mDate = date;
        notifyPropertyChanged(BR._all);
    }

    public void setTime(String time) {
        mTime = time;
        notifyPropertyChanged(BR._all);
    }

    public void setGradient(int[] gradient) {
        mGradient = gradient;
        notifyPropertyChanged(BR._all);
    }

    public void setIcon(int icon) {
        mIcon = icon;
        notifyPropertyChanged(BR._all);
    }

    public void setDoneIndex(int doneIndex) {
        mDoneIndex = doneIndex;
        notifyPropertyChanged(BR._all);
    }
}
